/*
 * usart.c
 *
 *  Created on: 15.09.2017
 *      Author: annetta897
 */
#include <avr/io.h>

#ifndef F_CPU
	#define F_CPU 1000000UL
#endif

#ifndef BAUD_RATE
	#define BAUD_RATE 9600UL
#endif

#define UBRR_VAL F_CPU/(8*BAUD_RATE)-1

void USARTInit (void)
{
	UBRR1H = UBRR_VAL>>8;
	UBRR1L = UBRR_VAL;
	UCSR1A |= (1<<U2X);
	UCSR1C |= (1<<UCSZ1)|(1<<UCSZ0);
	UCSR1B |= (1<<TXEN);
}

void USARTSend (uint8_t * data, uint8_t count)
{
	uint8_t i;
	for (i = 0; i < count; i++){
		while (!(UCSR1A & (1<<UDRE)));
		UDR1 = data[i];
	}
}

