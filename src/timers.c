/*
 * timers.c
 *
 *  Created on: 15.09.2017
 *      Author: annetta897
 */
#include <avr/io.h>

#ifndef F_CPU
	#define F_CPU 1000000UL
#endif

void TIM1Init (void)
{
	TCCR1B |= (1<<CS12); //prescaler = 256
}

void TIM1SetVal (uint32_t us)
{
	uint16_t val = us >> 8;
	TCNT1H = val >> 8;
	TCNT1L = val;
}


uint32_t TIM1GetVal (void)
{
	uint16_t val;
	val = TCNT1L;
	val |= (TCNT1H << 8);
	return (uint32_t)val << 8;
}

