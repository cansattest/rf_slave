/*
 * main.c
 *
 *  Created on: 20.09.2017
 *      Author: annetta897
 */
#include <avr/io.h>
#include <usart.h>
#include <timers.h>
#include <nrf.h>
#include <util/delay.h>

void ReverseString(uint8_t * str, uint8_t cnt)
{
	uint8_t i, t;
	for (i = 0; i < cnt/2; i ++){
		t = str[i];
		str[i] = str[cnt-1-i];
		str[cnt-1-i] = t;
	}

}

int main (void)
{
	uint8_t data[PAYLOAD_LENGTH];
	USARTInit();
	_delay_ms(200);
	RadioConfig (receive);
	while (1){
		if (RadioDataReceived()){
			RadioReadData (data);
			USARTSend (data, PAYLOAD_LENGTH);
			ReverseString (data, PAYLOAD_LENGTH);
			_delay_ms (500);
			RadioConfig (transmitt);
			RadioSendCmd (FLUSH_TX, 0, 0, 0);
			RadioSetRegVal (STATUS, 0x7F);
			RadioWriteData (data);
			while (!(RadioGetStatus() & ((1<<4)|(1<<5))))
				;
			RadioConfig (receive);
		}
	}
	return 0;
}

