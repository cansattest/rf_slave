/*
 * nrf.h
 *
 *  Created on: 19.09.2017
 *      Author: annetta897
 */
#include <inttypes.h>

#ifndef NRF_H_
#define NRF_H_

//CMDs
#define R_REGISTER 0x00
#define W_REGISTER 0x20
#define R_RX_PAYLOAD 0x61
#define W_TX_PAYLOAD 0xA0
#define FLUSH_TX 0xE1
#define FLUSH_RX 0xE2
#define REUSE_TX_PL 0xE3
#define R_RX_PL_WIDTH 0x60
#define W_ACK_PAYLOAD 0xA8
#define W_TX_PAYLOAD_NO_ACK 0xB0
#define NOP 0xFF

//Registers
#define CONFIG 0x00
#define EN_AA 0x01
#define EN_RXADDR 0x02
#define SETUP_AW 0x03
#define SETUP_RETR 0x04
#define RF_CH 0x05
#define RF_SETUP 0x06
#define STATUS 0x07
#define OBSERVE_TX 0x08
#define RPD 0x09
#define RX_ADDR(x) (0x0A+(x))
#define TX_ADDR 0x10
#define RX_PW_P(x) (0x11+(x))
#define FIFO_STATUS 0x17
#define DYNPD 0x1C
#define FEATURE 0x1D

#define RF_CH_VAL 0
#define PAYLOAD_LENGTH 14

typedef struct{
	uint8_t addr;
	uint8_t data;
} NRF_config_t;

typedef enum {
	transmitt,
	receive,
} mode_t;

void RadioPortsConfig (void);
void RadioConfig (mode_t);
uint8_t RadioSendCmd (uint8_t, uint8_t *, uint8_t *, uint8_t);
uint8_t RadioGetStatus (void);
uint8_t RadioSetRegVal (uint8_t, uint8_t);
uint8_t RadioGetRegVal (uint8_t);
void RadioWriteData (uint8_t *);
void RadioReadData (uint8_t *);
int RadioDataSent (void);
int RadioDataReceived (void);

#endif /* NRF_H_ */
